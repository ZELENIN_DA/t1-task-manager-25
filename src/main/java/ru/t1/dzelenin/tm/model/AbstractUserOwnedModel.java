package ru.t1.dzelenin.tm.model;

import org.jetbrains.annotations.Nullable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class AbstractUserOwnedModel extends AbstractModel {

    @Nullable
    private String userId;

}

